import time
import datetime

from influxdb import InfluxDBClient
client = InfluxDBClient(host='localhost', port=8086)
client.switch_database('nist_scenario')

i=0
current=datetime.datetime(2018,11,2,13,0,0)
while(i<70):
	print(i)
	json_body = [
		{
			"measurement": "scenario2",
			"time": current.strftime('%Y-%m-%dT%H:%M:%SZ'),
			"fields": {
				"speed": 50+i,
				"dist":100+i,
				"wait":150+i,
				"fuel":200+i
			}
		}
	]
	client.write_points(json_body)
	i+=1
	current=current+datetime.timedelta(seconds=100)
	time.sleep(.01)