import time
import datetime
import csv

from influxdb import InfluxDBClient
client = InfluxDBClient(host='localhost', port=8086)
client.switch_database('nist_scenario')

i=0
current=datetime.datetime(2018,11,2,13,0,0)
first=-1
with open('baseScenario.csv') as data:
	reader=csv.reader(data)
	for row in data:
		first+=1
		if (first>0):
			columns=row.split(',')
			print(row)
			print(i)
			json_body = [
				{
					"measurement": "scenario1",
					"time": current.strftime('%Y-%m-%dT%H:%M:%SZ'),
					"fields": {
						"speed": float(columns[1]),
						"dist":float(columns[2]),
						"wait":float(columns[3]),
						"fuel":float(columns[4])
					}
				}
			]
			client.write_points(json_body)
			i+=1
			current=current+datetime.timedelta(seconds=100)
			time.sleep(.1)
			
i=0
current=datetime.datetime(2018,11,2,13,0,0)
first=-1
with open('DOSScenario.csv') as data:
	reader=csv.reader(data)
	for row in data:
		first+=1
		if (first>0):
			columns=row.split(',')
			print(row)
			print(i)
			json_body = [
				{
					"measurement": "scenario2",
					"time": current.strftime('%Y-%m-%dT%H:%M:%SZ'),
					"fields": {
						"speed": float(columns[1]),
						"dist":float(columns[2]),
						"wait":float(columns[3]),
						"fuel":float(columns[4])
					}
				}
			]
			client.write_points(json_body)
			i+=1
			current=current+datetime.timedelta(seconds=100)
			time.sleep(.1)
			
i=0
current=datetime.datetime(2018,11,2,13,0,0)
first=-1
with open('integrityScenario.csv') as data:
	reader=csv.reader(data)
	for row in data:
		first+=1
		if (first>0):
			columns=row.split(',')
			print(row)
			print(i)
			json_body = [
				{
					"measurement": "scenario3",
					"time": current.strftime('%Y-%m-%dT%H:%M:%SZ'),
					"fields": {
						"speed": float(columns[1]),
						"dist":float(columns[2]),
						"wait":float(columns[3]),
						"fuel":float(columns[4])
					}
				}
			]
			client.write_points(json_body)
			i+=1
			current=current+datetime.timedelta(seconds=100)
			time.sleep(.1)